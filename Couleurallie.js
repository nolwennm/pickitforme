/////////////////////////////////////////////////
////////////***/ CouleurAllié /***///////////////
/////////////////////////////////////////////////
/// Designed and programmed by Nolwenn Maudet ///
/////////////////////////////////////////////////


// To do
/////////////////////
//permettre de créer une nouvelle node à partir d'un code couleur
//permettre de d'interpoler entre deux couleurs
//fix position bug on chrome
//prendre en compte la taille dans la génération des couleurs




// let's setup useful variables and objects here
/////////////////////
var myNodes = new Object();
var mousePos = {
    posX: 0,
    posY: 0,
}
var myTimer;
var currentlySelectedNode = undefined;
var saving = false;
var zIndex = 1;
var modedemploiOn = true;
var modedemploiOpacity = 1;
var nbSavingDays = 350;


function Node(x, y, size, colorR, colorG, colorB) {

	this.x = x;
	this.y = y;
	this.size = size;

	this.colorR = colorR;
    this.colorG = colorG;
    this.colorB = colorB;

    isSelected = 0;

}

// now onto the functions
/////////////////////
function onDocumentReady(){
    //let's add the event handling nodes saving via cookies
    window.addEventListener("beforeunload", onDocumentClosing);

	//if we don't have any cookie, let's initiate our two initial nodes
    //initializeNodes();
    if(getCookie("SavedNodes") == ""){
        console.log("I don't have any cookie");
        initializeNodes();
    }
    else{
        //we keep on saving
        saving = true;
        savingButton = document.getElementById("saving");
        savingButton.classList.remove('savingOFF');
        savingButton.classList.add('savingON');
        //we assume the person knows how to use couleurallie so we remove the modedemploi
        var modedemploi = document.getElementById("modedemploi");
        modedemploi.style.opacity = "0";
        modedemploiOn = false;

        //we retrieve and load the cookie content
        var cookieContent = getCookie("SavedNodes");
        myNodes = JSON.parse(cookieContent);

        for(var i=0; i<=Object.keys(myNodes).length; i++){
            var newNodeId = "node" + i;
            if (myNodes[newNodeId]!=undefined){
                drawHTMLNode(newNodeId, myNodes[newNodeId].x, myNodes[newNodeId].y, myNodes[newNodeId].size, myNodes[newNodeId].colorR, myNodes[newNodeId].colorG, myNodes[newNodeId].colorB);
            }
        }
        console.log("I've re-created the nodes from the cookie");
    }
};

function initializeNodes(){
    //we first make sure we don't have any nodes left
    resetMyNodes();
    //we now recreate all the nodes
    for(var i=0; i<2; i++){
        var newNodeId = "node" + i;
        var size = 25*(i+1);
        drawHTMLNode(newNodeId, (window.innerWidth/2 - size/2), 80*i+150, size, 0, 0, 0);
        var newHTMLnode = document.getElementById(newNodeId);

        if(i==0){
            newHTMLnode.style.backgroundColor = "rgb(0,0,0)";
            //create node object
            var newNode = new Node(window.innerWidth/2-size/2, 80*i+150, size, 0, 0, 0);
        }
        else{
            newHTMLnode.style.backgroundColor = "rgb(255,255,255)";
            //create node object
            var newNode = new Node(window.innerWidth/2-size/2, 80*i+150, size, 255, 255, 255);
        }
        myNodes[newNodeId] = newNode;
    }
    console.log("I've created two initial nodes");
};

function drawHTMLNode(newNodeId, x, y, size, colorR, colorG, colorB){
    var newHTMLnode = document.createElement("DIV");
    newHTMLnode.id = newNodeId;
    newHTMLnode.classList.add('node');
    newHTMLnode.style.left = x + "px";
    newHTMLnode.style.top = y + "px";
    newHTMLnode.style.width = size + "px";
    newHTMLnode.style.height = size + "px";
    newHTMLnode.style.backgroundColor = "rgb("+colorR+","+colorG+","+colorB+")";
    newHTMLnode.style.zIndex = "1";
    newHTMLnode.addEventListener("mousedown", mouseDownNode);
    newHTMLnode.addEventListener("mouseup", clickOnNode);
    document.body.appendChild(newHTMLnode);
}

function resetMyNodes(){
    //we first to delete all the HTML nodes
    for(var key in myNodes){
        if(myNodes[key]!=undefined){
            removeHtmlElement(key);
        }
    }
    //we now reset the node object
    for (var key in myNodes) {
        delete myNodes[key];
    }  
};

function savingBool(){
    savingButton = document.getElementById("saving");
    if(saving==true){
        saving=false;
        savingButton.classList.remove('savingON');
        savingButton.classList.add('savingOFF');
    }
    else{
        saving=true;
        savingButton.classList.remove('savingOFF');
        savingButton.classList.add('savingON');
    }
};

function onDocumentClosing(){
    document.cookie = "SavedNodes" + '=; expires=Thu, 01 Jan 1970 00:00:01 GMT;path=/; SameSite=Strict';
    if(saving==true){
        var nodeObjects = JSON.stringify(myNodes);
        setCookie("SavedNodes", nodeObjects, nbSavingDays);
    }
};

function handleNodeDrag(myNodeId){
    console.log("im in handleNodeDrag");
    var moveNode = function(e){
        if((event.pageX - myNodes[myNodeId].size/2)>(window.innerWidth-myNodes[myNodeId].size-5)){
            myNodes[myNodeId].x = window.innerWidth-myNodes[myNodeId].size-5;
        }
        else{
            myNodes[myNodeId].x = event.pageX - myNodes[myNodeId].size/2;
        }
        if((event.pageY - myNodes[myNodeId].size/2)>(window.innerHeight-myNodes[myNodeId].size-5)){
            myNodes[myNodeId].y = window.innerHeight-myNodes[myNodeId].size-5;
        }
        else{
            myNodes[myNodeId].y = event.pageY - myNodes[myNodeId].size/2;
        }
        var myHTMLnode = document.getElementById(myNodeId); 
        myHTMLnode.style.left = myNodes[myNodeId].x + "px";
        myHTMLnode.style.top = myNodes[myNodeId].y + "px";
    };

    var dropNode = function (){
        document.removeEventListener("mousemove", moveNode);
    };

    document.addEventListener("mousemove", moveNode);
    document.addEventListener("mouseup", dropNode, {once: true});

};

function mouseDownNode(e){
    //we prevent event bubbling
    event.stopPropagation();

    var myNodeId = e.target.id
    //we deselect already selected node and assign the new one
    if(currentlySelectedNode != undefined){
        deselectNode(currentlySelectedNode);
        stopNodeGeneration();
    };
    if (currentlySelectedNode != myNodeId){
        selectNode(myNodeId);
        currentlySelectedNode = myNodeId;
    }
    else{
        currentlySelectedNode = undefined;
    }
    
    //we increase selectedNode size
    changeNodeSize(myNodeId, 5);

    //initiate node dragging mechanism
    handleNodeDrag(myNodeId);
    
};

function clickOnNode(e){
    var myNodeId = e.target.id

    if(currentlySelectedNode == myNodeId){
        var myHTMLnode = document.getElementById(myNodeId); 
        //we add eventlistener for node generation when we exit the selected node
        myHTMLnode.addEventListener("mouseout", activateNodeGeneration, {once: true});
    }
};

function deselectNode(myNodeId){
           
    //we remove the centerNode
    var myCenterNodeId = "center" + myNodeId;
    removeHtmlElement(myCenterNodeId);
    //we remove the eventlisteners
    var myHTMLnode = document.getElementById(myNodeId);
    myHTMLnode.removeEventListener("mouseout", activateNodeGeneration);
    document.body.removeEventListener("mousemove", drawBlackLine); 
    //we remove the blackLine
    mySVG = document.getElementById("mySVG");
    myLine = document.getElementById("line");
    mySVG.removeChild(myLine);
}

function selectNode(myNodeId){

    var myHTMLnode = document.getElementById(myNodeId);
    zIndex = zIndex+1;
    myHTMLnode.style.zIndex = zIndex;

    //we create the centerNode if it does not already exist
    var centerNodeId = "center"+myNodeId;
    if (!document.getElementById(centerNodeId)){
        var centerNode = document.createElement("DIV");
        centerNode.id = "center"+myNodeId;
        centerNode.classList.add('centerNode');
        centerNode.style.left = myNodes[myNodeId].size/2 + "px";
        centerNode.style.top = myNodes[myNodeId].size/2 + "px";
        centerNode.style.zIndex = 4;
        centerNode.style.pointerEvents = "none";
        myHTMLnode.appendChild(centerNode);

        //We adjust the centerNode color based on selectedNode using YIQ
        var sourceColorR = myNodes[myNodeId].colorR;
        var sourceColorG = myNodes[myNodeId].colorG;
        var sourceColorB = myNodes[myNodeId].colorB;
        var yiq = ((sourceColorR*299)+(sourceColorG*587)+(sourceColorB*114))/1000;
        if( yiq >= 128){
            centerNode.style.backgroundColor = "rgb(0,0,0)";
        }
        else{
            centerNode.style.backgroundColor = "rgb(255,255,255)";
        };
    }

    //we prepare for drawing the black line between the cursor and the node 
    instanciateBlackLine(myNodeId);

    //we prepare for displaying the selected color in the panel
    displayColor(myNodeId);
}

function displayColor(myNodeId){
    var sourceColorR = myNodes[myNodeId].colorR;
    var sourceColorG = myNodes[myNodeId].colorG;
    var sourceColorB = myNodes[myNodeId].colorB;

    //we display the color values in the panel
    var swatch = document.getElementById("swatch");
    var hexaValueBox = document.getElementById("hexaValue");
    var hexValue = rgbToHex(sourceColorR, sourceColorG, sourceColorB);
    var rValueBox = document.getElementById("rValue");
    var gValueBox = document.getElementById("gValue");
    var bValueBox = document.getElementById("bValue");

    swatch.style.backgroundColor = hexValue;
    rValueBox.value = sourceColorR;
    gValueBox.value = sourceColorG;
    bValueBox.value = sourceColorB;
    hexaValueBox.value = hexValue;

}

function instanciateBlackLine (myNodeId){
    //we prepare for drawing the black line between the cursor and the node 
    //we add an eventlistener to the canvas to check mousemovement
    document.body.addEventListener("mousemove", drawBlackLine); 
    //we add myNodeId as a parameter to fetch it inside the function
    document.body.myNodeId = myNodeId;
    //we create the svg line
    var newLine = document.createElementNS('http://www.w3.org/2000/svg','line');
    newLine.setAttribute('id','line');
    newLine.setAttribute("stroke", "black");
    mySVG = document.getElementById("mySVG");
    mySVG.append(newLine);
}

var drawBlackLine = function(event){
    myNodeId = event.currentTarget.myNodeId;

    //updating the line's position
    myLine = document.getElementById("line");
    var x1 = myNodes[myNodeId].x+myNodes[myNodeId].size/2;
    var y1 = myNodes[myNodeId].y+myNodes[myNodeId].size/2;
    var x2 = mousePos.posX;
    var y2 = mousePos.posY;
    myLine.setAttribute('x1', x1);
    myLine.setAttribute('y1',myNodes[myNodeId].y+myNodes[myNodeId].size/2);
    myLine.setAttribute('x2',mousePos.posX);
    myLine.setAttribute('y2',mousePos.posY);
    //updating the line's color
    var distance = Math.sqrt(Math.pow((x2-x1), 2) + Math.pow((y2-y1), 2));
    var lineOpacity = mapRange(distance, 0, window.innerWidth, 0.2, 1);
    myLine.setAttribute('opacity', lineOpacity);
    
}

function activateNodeGeneration (e){
    //initiate the node creation timer
    myTimer = setInterval(function(){createNode(e.target.id);}, 500);

};

function createNode (selectedNodeId){
    //we remove the activateNodeGeneration event listener
    var myHTMLnode = document.getElementById(selectedNodeId);
    myHTMLnode.removeEventListener("mouseout", activateNodeGeneration, false);

    //we get the distance between node and cursor
    var distance = Math.hypot(mousePos.posX - (myNodes[selectedNodeId].x+myNodes[selectedNodeId].size/2), mousePos.posY - (myNodes[selectedNodeId].y+myNodes[selectedNodeId].size/2));

    //get source colors
    var sourceColorR = myNodes[selectedNodeId].colorR;
    var sourceColorG = myNodes[selectedNodeId].colorG;
    var sourceColorB = myNodes[selectedNodeId].colorB;

    //we generate random color based on distance and constrained between 0 & 255
    var random = 0;
    random = Math.floor(Math.random() * (distance + distance + 1)) - distance;
    var newColorR = Math.round(Math.max(Math.min(sourceColorR + random, 255), 0));
    random = Math.floor(Math.random() * (distance + distance + 1)) - distance;
    var newColorG = Math.round(Math.max(Math.min(sourceColorG + random, 255), 0));
    random = Math.floor(Math.random() * (distance + distance + 1)) - distance;
    var newColorB = Math.round(Math.max(Math.min(sourceColorB + random, 255), 0));

    //generate positon for the new node, making sure we don't go offscreen
    var windowWidth = window.innerWidth;
    var windowHeight = window.innerHeight;
    var newx = windowWidth+1;
    var newy = windowHeight+1;
    while((newx>(windowWidth-60))||(newy>(windowHeight-60))||(newx<20)||(newy<20)){
        var angle = Math.random()*2*Math.PI;
        newx = Math.round(Math.cos(angle)*distance+(myNodes[selectedNodeId].x+myNodes[selectedNodeId].size/2-25));
        newy = Math.round(Math.sin(angle)*distance+(myNodes[selectedNodeId].y+myNodes[selectedNodeId].size/2-25));
    };

    //create new node object using generated values
    //if we have an undefined node in our Nodes collection, we will create the new node there
    var newNode = new Node(newx, newy, 50, newColorR, newColorG, newColorB);
    var i = 0;
    var newNodeId = "node" + i;
    while ((i>=Object.keys(myNodes).length+1) || (myNodes[newNodeId]!=undefined)){
        i++;
        newNodeId = "node" + i;
    }
    myNodes[newNodeId] = newNode;

    //draw new node in HTML
    drawHTMLNode(newNodeId, newx, newy, 50, newColorR, newColorG, newColorB);

    //shrink all nodes
    shrinkNode(selectedNodeId);

    //we gradually fade modedemploi
    if(modedemploiOn){
        fadeModedemploi();
    }
};

function fadeModedemploi(){
    console.log("im in fadeModedemploi");
    modedemploiOpacity -= 0.1;
    var modedemploi = document.getElementById("modedemploi");
    modedemploi.style.opacity = modedemploiOpacity;
    if(modedemploiOpacity === 0){
        modedemploiOn = false;
    }
}

function shrinkNode(selectedNodeId){

    for(var i=0; i<Object.keys(myNodes).length-1; i++){
        var myNodeId = "node" + i;
        if((myNodeId != selectedNodeId) && (myNodes[myNodeId] != undefined)){
            changeNodeSize(myNodeId, -1);
        }
    }
};

function changeNodeSize(nodeId, factor){
    //we make sure that the node stays centered
    //first the model
    var myHTMLnode = document.getElementById(nodeId);
    var myNodeNewSize = myNodes[nodeId].size +=factor;
    myNodes[nodeId].x +=(-factor/2);
    myNodes[nodeId].y +=(-factor/2);

    if(myNodeNewSize>2){
        //then we update the corresponding html node
        myHTMLnode.style.width = myNodeNewSize +'px';
        myHTMLnode.style.height = myNodeNewSize +'px';
        myHTMLnode.style.left = myNodes[nodeId].x + "px";
        myHTMLnode.style.top = myNodes[nodeId].y + "px";
    }
    else{
        //if the node is too small, we remove it, in the model, and in the DOM
        removeNode(nodeId);
        removeHtmlElement(nodeId);
    }
};

function removeNode(myNodeId){
    myNodes[myNodeId] = undefined;
}

function stopNodeGeneration(){
    clearInterval(myTimer);
};

function clickedOnNothing(){
    //we deselect currentlySelectedNode and stop node generation
    if(currentlySelectedNode != undefined){
        deselectNode(currentlySelectedNode);
        stopNodeGeneration();
    };
    currentlySelectedNode = undefined;
};


// helper functions
/////////////////////

function getCursorPos(event){
    mousePos.posX = event.pageX;
    mousePos.posY = event.pageY;
};

function removeHtmlElement(id) {
    var element = document.getElementById(id);
    return element.parentNode.removeChild(element);
};

function componentToHex(c) {
  var hex = c.toString(16);
  return hex.length == 1 ? "0" + hex : hex;
}

function rgbToHex(r, g, b) {
  return "#" + componentToHex(r).toUpperCase() + componentToHex(g).toUpperCase() + componentToHex(b).toUpperCase();
}

function mapRange(value, low1, high1, low2, high2) {
    return low2 + (high2 - low2) * (value - low1) / (high1 - low1);
}

// cookie handling functions
////////////////////////////

function setCookie(name, value, numberdays) {
  var d = new Date();
  d.setTime(d.getTime() + (numberdays*24*60*60*1000));
  var expires = "expires="+ d.toUTCString();
  document.cookie = name + "=" + value + ";" + expires + ";path=/";
}

function getCookie(name) {
  var name = name + "=";
  var decodedCookie = decodeURIComponent(document.cookie);
  var ca = decodedCookie.split(';');
  for(var i = 0; i <ca.length; i++) {
    var c = ca[i];
    while (c.charAt(0) == ' ') {
      c = c.substring(1);
    }
    if (c.indexOf(name) == 0) {
      return c.substring(name.length, c.length);
    }
  }
  return "";
}

